package ui;

import Exceptions.InsufficientBalanceException;
import Exceptions.InvalidFeeException;
import at.hakimst.sample.WalletApp;
import domain.CryptoCurrency;
import domain.Wallet;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;

import java.math.BigDecimal;
import java.util.Optional;

// Die Klasse MainController implementiert den Logik-Teil!!
// Enthält den CODE
public class MainController extends BaseControllerState {

    // VIDEO S: MainController
    // Implementierung der fx:id Elemente durch setzen entsprechender Datenfelder
    @FXML
    private Button btnClose;
    @FXML
    private ComboBox cmbWalletCurrency;
    @FXML
    private Label lblBankaccountBalance;
    @FXML
    private TableView<Wallet> tableView;

    //Video R: auskommentieren
    //private final String javafxVersion = System.getProperty("javafx.version");

    // Deklarieren von Datenfelder: richtiger Typ & Name
    // Verbindung vom FXML-File (=main.fxml) und dem Code-Behind (=MainController)
    /*
    @FXML
    private Label color;

    @FXML
    private Label label;

    @FXML
    private ResourceBundle resources;
    */

    // Life-Cycle-Methode wenn der FXML-Loader das erste mal als Objekt erzeugt
    // Ähnlich wie ein Konstruktor
    // mit rescoures.getString holt er sich zum key=label.text den entsprechenden text = value
    // Video R: initialize stehen lassen!!!
    // Befüllt den Code, arbeitet im Background und kann ruhig leer sein!!!
    public void initialize() {
        //Video T:
        //Liefert uns eine List von Codes aus unserer ENUM Klasse CryptoCurrency
        //Hinzufügen in unser DropDown (ComboBox)
        this.cmbWalletCurrency.getItems().addAll(CryptoCurrency.getCodes());

        // Globaler Speicher:Global BankAccount aus der Main Klasse / getBankAccount aus der BaseControllerState Klasse
        // Am BankAccount kann ich dann die Methode getBalance aufrufen und sie in einen String umwandeln!
        this.lblBankaccountBalance.textProperty().setValue(getBankAccount().getBalance().toString());

        //Video T:
        //TableView der Wallet Klasse
        //PropertyValueFactory = Fabrik -> Holt einen Wert aus einem Datenfeld
        //Property = Er greift die getter-Methoden ab (in JAVA_FX)
        TableColumn<Wallet,String> symbol = new TableColumn<>("SYMBOL");
        symbol.setCellValueFactory(new PropertyValueFactory<>("cryptoCurrency"));   //Datenfeld in der Klasse Wallet

        TableColumn<Wallet,String> currencyName = new TableColumn<>("CURRENCY NAME");
        currencyName.setCellValueFactory(new PropertyValueFactory<>("currencyName"));   //Datenfeld in der Klasse Wallet

        TableColumn<Wallet,String> name = new TableColumn<>("WALLET NAME");
        name.setCellValueFactory(new PropertyValueFactory<>("name"));   //Datenfeld in der Klasse Wallet

        TableColumn<Wallet,String> amount = new TableColumn<>("AMOUNT");
        amount.setCellValueFactory(new PropertyValueFactory<>("amount"));   //Datenfeld in der Klasse Wallet

        //Damit die Spalten angezeigt werden:
        tableView.getColumns().clear();
        tableView.getColumns().add(name);
        tableView.getColumns().add(symbol);
        tableView.getColumns().add(currencyName);
        tableView.getColumns().add(amount);

        //Mit set werden Einträge definiert:
        tableView.getItems().setAll(getWalletList().getWalletsAsObservableList());

        //Video Z:
        //EXIT BUTTON konfigurieren
        this.btnClose.setOnAction((ActionEvent event)->
        {
            //Beendet die Applikation (CLOSE BUTTON)
            Platform.exit();
        });
    }

    public void deposit(){
        //Arbeiten mit Dialog Fenster die am Bildschirm erscheinen
        TextInputDialog dialog = new TextInputDialog("Insert amount to deposit ...");
        dialog.setTitle("Deposit to bankaccount");
        dialog.setHeaderText("How much money do you want to deposit?");
        dialog.setContentText("Amount: ");
        Optional<String> result = dialog.showAndWait();   //Liefer ein Optional zurück! Bedeutet: Ist ein Wert gekommen oder nicht?!
        //Wenn das result nicht 0 ist: BigDecimal amount = new BigDecimal(result.get())
        if(result.isPresent())
        {
            try {
                BigDecimal amount = new BigDecimal(result.get());
                //GlobalState
                this.getBankAccount().deposit(amount);
                this.lblBankaccountBalance.textProperty().set(this.getBankAccount().getBalance().toString());
            }catch (NumberFormatException numberFormatException){
                WalletApp.showErrorDialog("Please insert a number!");
            }
        }
    }

    public void withdraw(){
        //Arbeiten mit Dialog Fenster die am Bildschirm erscheinen
        TextInputDialog dialog = new TextInputDialog("Insert amount to withdraw ...");
        dialog.setTitle("Withdraw to bankaccount");
        dialog.setHeaderText("How much money do you want to withdraw?");
        dialog.setContentText("Amount: ");
        Optional<String> result = dialog.showAndWait();   //Liefer ein Optional zurück! Bedeutet: Ist ein Wert gekommen oder nicht?!
        //Wenn das result nicht 0 ist: BigDecimal amount = new BigDecimal(result.get())
        if(result.isPresent())
        {
            try {
                BigDecimal amount = new BigDecimal(result.get());
                //GlobalState
                this.getBankAccount().withdraw(amount);
                this.lblBankaccountBalance.textProperty().set(this.getBankAccount().getBalance().toString());
            }catch (NumberFormatException numberFormatException){
                WalletApp.showErrorDialog("Please insert a number!");
            } catch (InsufficientBalanceException insufficientBalanceException){
                WalletApp.showErrorDialog(insufficientBalanceException.getMessage());
                //Nutzen unsere eigene Exception Message der InsufficientBalanceException
            }
        }
    }
    //Video V:
    //Aus der TableView das selektierte Wallet herausholen, in den Globalen Zustand speichern und dann die Szene wechseln!
    public void openWallet(){

        //Da wir oben ein Datenfeld vom Typ Tableview of Wallets haben TableView<Wallet>
        Wallet wallet = this.tableView.getSelectionModel().getSelectedItem();
        //Nur wenn der User etwas ausgewählt hat!
        if(wallet != null){
            //Nimmt die wallet und schieb sie in den Globalen Zustand hinein
            //damit wir sie aus dem WalletController heraus holen können
            //dann erst der Szenen Wechsel
            GlobalContext.getGlobalContext().putStateFor(WalletApp.GLOBAL_SELECTED_WALLET,wallet);
            WalletApp.switchScene("wallet.fxml","at.hakimst.sample.wallet");
        } else
        {
            WalletApp.showErrorDialog("You have to select a wallet first!");
        }


    }

    //Video T:
    public void newWallet() throws InvalidFeeException { //Betrifft eher den Programmierer da wir die feeInPercent selbst festlegen!!

        Object selectedItem = this.cmbWalletCurrency.getSelectionModel().getSelectedItem();
        if(selectedItem == null){
            WalletApp.showErrorDialog("Choose currency!");
            return;
        }
        //Auf Basis der in der ENUM Klasse CryptoCurrency hinterlegten Values
        CryptoCurrency selectedCryptoCurrency = CryptoCurrency.valueOf(this.cmbWalletCurrency.getSelectionModel().getSelectedItem().toString());
        this.getWalletList().addWallet(new Wallet("My " + selectedCryptoCurrency.currencyName + " Wallet",selectedCryptoCurrency,new BigDecimal(1)));
        //Holen aus der globalen Liste und setzen die Items neu!!
        tableView.getItems().setAll(this.getWalletList().getWalletsAsObservableList());
    }

    /*
    public void initialize() {
        label.setText(String.format(resources.getString("label.text"), "JavaFX", javafxVersion));
    }

    @FXML
    private void updateColor() {
        Random random = new Random();
        this.color.setTextFill(Color.rgb(random.nextInt(256), random.nextInt(256), random.nextInt(256)));
    }
     */
}
