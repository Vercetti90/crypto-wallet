package ui;

import Exceptions.GetCurrentPriceException;
import Exceptions.InsufficientAmountException;
import Exceptions.InsufficientBalanceException;
import Exceptions.InvalidAmountException;
import at.hakimst.sample.WalletApp;
import domain.CurrentPriceForCurrency;
import domain.Transaction;
import domain.Wallet;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Optional;


public class WalletController extends BaseControllerState {

    //BackToMain Button:
    //Variante 1: onAction="back" hier einfügen         <Button fx:id="btnBackToMain" text="%btnBack.text" styleClass="naviButton"/>
    //Variante 2: siehe initialize-Methode
    @FXML
    private Button btnBackToMain;   //javafx.scene.control.Button;
    @FXML
    private Label lblId, lblName, lblCurrency, lblAmount, lblFee, lblValue;
    @FXML
    private TableView<Transaction> tblTransactions;

    private Wallet wallet;

    //Erstellt neuen Controller
    //Und diese initialize-Methode wird dann als Life-Cycle-Methode ausgeführt
    //Ähnlich wie ein Konstruktor
    public void initialize() {
        //Hier: Wallet aus dem globalen Kontext, aus der MainController Klasse, Methode openWallet()
        this.wallet = (Wallet) GlobalContext.getGlobalContext().getStateFor(WalletApp.GLOBAL_SELECTED_WALLET);

        // Vor Refresh aurufen , weil wir die Tabelle auch refreshen müssen!
        populateTable();
        //Refresh aller Einträge
        refreshAllGuiValues();

        //Aufpassen import ActionEvent aus java.fx
        //Wenn ein Button gedrückt wird -> Event-Handling-Methode
        btnBackToMain.setOnAction((ActionEvent e) ->
        {
            WalletApp.switchScene("main.fxml", "at.hakimst.sample.main");
        });
    }

    //Video X:
    //Liefert currenPriceStragy zurück
    private CurrentPriceForCurrency getCurrentPriceStrategy() {

        return (CurrentPriceForCurrency) GlobalContext.getGlobalContext().getStateFor(WalletApp.GLOBAL_CURRENT_CURRENCY_PRICES);
    }

    //Video X:
    //Wird ausgeführt wenn wir die initialize-Methoden ausführen
    private void refreshAllGuiValues() {

        //Setzen des Names des Wallets
        this.lblName.textProperty().setValue(this.wallet.getName());
        this.lblId.textProperty().setValue(this.wallet.getId().toString());
        this.lblCurrency.textProperty().setValue(wallet.getCryptoCurrency().toString());
        this.lblAmount.textProperty().setValue(wallet.getAmount().toString());
        this.lblFee.textProperty().setValue(wallet.getFeeInPercent().toString());

        //Wert des Wallets
        //Strategie wird in der Klasse WalletApp gesetzt unter GlobalContext

        //CurrentPriceForCurrency currentPriceStrategy = (CurrentPriceForCurrency) GlobalContext.getGlobalContext().getStateFor(WalletApp.GLOBAL_CURRENT_CURRENCY_PRICES);

        //Erstellung private Hilfmethode siehe oben : getCurrentPriceStrategy
        //(CurrentPriceForCurrency) GlobalContext.getGlobalContext().getStateFor(WalletApp.GLOBAL_CURRENT_CURRENCY_PRICES);
        //damit wir Sie nicht jedes mal schreiben müssen, da wir den currentprice öfters brauchen

        try {
            BigDecimal currentPrice = this.getCurrentPriceStrategy().getCurrentPrice(wallet.getCryptoCurrency());
            //Zum testen Video: X
            System.out.println(currentPrice);
            BigDecimal currentValue = currentPrice.multiply(wallet.getAmount()).setScale(6, RoundingMode.HALF_UP);
            //erst jetzt wenn wir das Label für den Value setzen!
            this.lblValue.textProperty().setValue(currentValue.toString());
        } catch (GetCurrentPriceException e) {
            WalletApp.showErrorDialog(e.getMessage());
            //Wenn das Setzen des Labels schief geht -> setzen auf irgendeinen Wert
            this.lblValue.textProperty().setValue("CURRENT PRICES UNAVAILABLE!");
            e.printStackTrace();
        }
        tblTransactions.getItems().setAll((wallet.getTransactions()));
    }

    //Video: Y
    private void populateTable() {
        //Name der ersten Spalte= ID
        TableColumn<Transaction, String> id = new TableColumn<>("ID");
        id.setCellValueFactory(new PropertyValueFactory<>("id"));
        //Transaktions-Klasse nachschauen ob alles vorhanden ist: z.B.: getID, getCryptoCurrency, getAmount...!
        //get wird weggelassen!!! z.B.: getId wird zu id!

        TableColumn<Transaction, String> crypto = new TableColumn<>("CRYPTO");
        crypto.setCellValueFactory(new PropertyValueFactory<>("cryptoCurrency"));

        TableColumn<Transaction, String> amount = new TableColumn<>("AMOUNT");
        amount.setCellValueFactory(new PropertyValueFactory<>("amount"));

        TableColumn<Transaction, String> total = new TableColumn<>("TOTAL");
        total.setCellValueFactory(new PropertyValueFactory<>("total"));

        TableColumn<Transaction, String> priceAtTransactionDate = new TableColumn<>("PRICE");
        priceAtTransactionDate.setCellValueFactory(new PropertyValueFactory<>("priceAtTransactionDate"));

        TableColumn<Transaction, String> date = new TableColumn<>("DATE");
        date.setCellValueFactory(new PropertyValueFactory<>("date"));

        //Der Tabelle hinzufügen
        tblTransactions.getColumns().clear();
        tblTransactions.getColumns().add(id);
        tblTransactions.getColumns().add(crypto);
        tblTransactions.getColumns().add(amount);
        tblTransactions.getColumns().add(total);
        tblTransactions.getColumns().add(priceAtTransactionDate);
        tblTransactions.getColumns().add(date);
    }

    //Video: Z
    public void buy() {

        TextInputDialog dialog = new TextInputDialog("Amount of crypto to buy ...");
        dialog.setTitle("Buy crypto");
        dialog.setHeaderText("How much crypto do you want to buy?");
        dialog.setContentText("Amount: ");
        //Benutzer gibt etwas ein: Entweder kriegen wir einen String oder nichts zurück!
        Optional<String> result = dialog.showAndWait();

        if (result.isPresent()) {
            try {
                BigDecimal amount = new BigDecimal(result.get());
                //sagen zum Wallet: JETZT KAUFEN!!
                //Extends WalletController BaseControllerState
                this.wallet.buy(amount, this.getCurrentPriceStrategy().getCurrentPrice(wallet.getCryptoCurrency()), this.getBankAccount());
                this.refreshAllGuiValues();
            } catch (NumberFormatException numberFormatException) {
                WalletApp.showErrorDialog("Invalid amount. Insert a number!");
            } catch (GetCurrentPriceException | InvalidAmountException | InsufficientBalanceException exception) {
                WalletApp.showErrorDialog(exception.getMessage());
                exception.printStackTrace();
            }
        }
    }

    //Video Z:
    public void sell() {

        TextInputDialog dialog = new TextInputDialog("Amount of crypto to sell ...");
        dialog.setTitle("Sell crypto");
        dialog.setHeaderText("How much crypto do you want to sell?");
        dialog.setContentText("Amount: ");
        //Benutzer gibt etwas ein: Entweder kriegen wir einen String oder nichts zurück!
        Optional<String> result = dialog.showAndWait();

        if (result.isPresent()) {
            try {
                BigDecimal amount = new BigDecimal(result.get());
                //sagen zum Wallet: JETZT VERKAUFEN!!
                //Extends WalletController BaseControllerState
                this.wallet.sell(amount, this.getCurrentPriceStrategy().getCurrentPrice(wallet.getCryptoCurrency()), this.getBankAccount());
                this.refreshAllGuiValues();
            } catch (NumberFormatException numberFormatException) {
                WalletApp.showErrorDialog("Invalid amount. Insert a number!");
            } catch (InvalidAmountException | GetCurrentPriceException | InsufficientAmountException exception) {
                WalletApp.showErrorDialog(exception.getMessage());
                exception.printStackTrace();
            }
        }
    }
}
