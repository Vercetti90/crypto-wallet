package ui;

import java.util.HashMap;

/**
 * Diese Klasse dient als Zustandsspeicher für unsere Applikation
 * Von dieser Klasse aus alle generierten Controller Objekte Zugriff haben!
  */
public class GlobalContext {

    private static GlobalContext globalContext; //Klassenvariable: privat, statisch
    //Object Obertyp von allen Klassen in JAVA
    //Können dadurch alle Objekte einfügen, müssen Sie nur zurück CASTEN!!!
    private HashMap<String,Object> states;      //Daten zwischen verschiedenen Controller austauschen
    // Bsp.:
    // hashmap.put(key:"Wallet", value:wallet);
    //In einem anderen Controller: (Wallet holen)
    // wallet = hashmap.get("Wallet")

    //Konstruktor: private = Nicht von außen erreichbar!
    private GlobalContext() {
        states = new HashMap<>();
    }

    //Methoden
    //Singleton-Pattern: Garantiert das die Instanz dieser Klasse nur ein einziges Mal erzeugt wird!
    public static GlobalContext getGlobalContext(){
        if(globalContext == null){
            globalContext = new GlobalContext();
        }
        return globalContext;
    }

    //Liefert uns ein Datenelement aus der HashMap
    public Object getStateFor(String key){
        return states.get(key);     //HashMap Methoden
    }

    public void putStateFor(String key, Object state){
        states.put(key,state);
    }

    public void removeStateFor(String key){
        states.remove(key);
    }

    public void emptyAllStates(){
        states.clear();
    }
}
