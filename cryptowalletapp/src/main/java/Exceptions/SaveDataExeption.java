package Exceptions;

public class SaveDataExeption extends Exception {
    public SaveDataExeption(String message){
        super(message);         //An die Mutterklasse übergeben, und dann evtl. über getMessage die Message abrufen!
    }
}
