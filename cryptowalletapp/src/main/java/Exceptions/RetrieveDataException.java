package Exceptions;

public class RetrieveDataException extends Exception {
    public RetrieveDataException (String message){
        super(message);     //An die Mutterklasse übergeben, und dann evtl. über getMessage die Message abrufen!
    }
}
