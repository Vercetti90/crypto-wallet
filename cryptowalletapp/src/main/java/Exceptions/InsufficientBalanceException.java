package Exceptions;

public class InsufficientBalanceException extends Exception {

    public InsufficientBalanceException() {
        super("Insufficient Account Balance"); // Aufruf der Superklasse Exception
                                               // insufficientBalanceException.getMessage() um diese Message auszugeben!
    }
}
