package at.hakimst.sample;

import Exceptions.RetrieveDataException;
import Exceptions.SaveDataExeption;
import Infrastructure.CurrentCurrencyPrices;
import Infrastructure.FileDataStore;
import domain.BankAccount;
import domain.DataStore;
import domain.WalletList;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.stage.Stage;
import ui.GlobalContext;

import java.io.IOException;
import java.util.ResourceBundle;

public class WalletApp extends Application {

    //Video M: Haupt-App-Klasse zum Hochfahren der GUI-Applikation
    //Einsprungspunkt
    //UI Parts
    //Wird intern benötigt darum private
    private static Stage mainStage; //Hauptbühne als private statische Variable

    //Video W: Für die Klasse WalletController
    public static String GLOBAL_SELECTED_WALLET = "selectedWallet";

    //Video O:
    public static final String GLOBAL_WALLET_LIST = "walletlist";
    public static final String GLOBAL_BANK_ACCOUNT = "bankaccount";
    public static final String GLOBAL_CURRENT_CURRENCY_PRICES = "currencyprices";
    //Methode um die Szene zu wechseln (Szenen Namen = fxmlFile)
    //FXMLLoader.load(WalletApp.class.getResource("main.fxml"),
    //                ResourceBundle.getBundle("at.hakimst.sample.main"))
    //in dieser Methode kapseln!!! (Wiederverwendbarkeit)

    //Wird extern benötigt darum public
    public static void switchScene(String fxmlFile, String resourceBundle) {
        try {
            Parent root = FXMLLoader.load(WalletApp.class.getResource(fxmlFile), ResourceBundle.getBundle(resourceBundle));
            Scene scene = new Scene(root);
            mainStage.setScene(scene);  //mainStage erhalten wir über die Methode start
            mainStage.show();
        }
          // NullPointerException sind "unchecked Exceptions" müssen nicht gecatched werden!!!
//        catch (NullPointerException nullPointerException) {   //Versucht Datei zu laden, mit einem Name den es nicht gibt!!
//            WalletApp.showErrorDialog("Could not load new scene!");
//            nullPointerException.printStackTrace();
//        }
        //IO-Exceptions sind verpflichtend!!!
        catch (Exception ioException) {
            WalletApp.showErrorDialog("Could not load new scene!");
            ioException.printStackTrace();
        }
    }

    public static void showErrorDialog(String message) {
        //Alert: Hier vom Typ ein ERROR-Dialog
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setHeaderText("An exception occurred: " + message);
        alert.showAndWait();    //Benutzer muss den Dialog wegklicken, sonst gehts nicht weiter
    }

    //Video N:
    //File-Handling Part
    private BankAccount loadBankAccountFromFile() throws RetrieveDataException{
        DataStore dataStore = new FileDataStore();
        BankAccount bankAccount = dataStore.retrieveBankAccount();
        System.out.println("Bankaccount loaded!");
        return bankAccount;
    }

    private WalletList loadWalletListFromFile() throws  RetrieveDataException{
        DataStore dataStore = new FileDataStore();
        WalletList walletList = dataStore.retrieveWalletList();
        System.out.println("WalletList loaded!");
        return walletList;
    }

    private void storeBankAccountToFile(BankAccount bankAccount) throws SaveDataExeption{
        DataStore dataStore = new FileDataStore();
        dataStore.saveBankAccount(bankAccount);
    }

    private void storeWalletListToFile(WalletList walletList) throws SaveDataExeption{
        DataStore dataStore = new FileDataStore();
        dataStore.saveWalletList(walletList);
    }

    //Methode start wird automatisch ausgeführt wenn die Applikation gestartet wird
    // -> 1.Life-Cycle-Method
    //Video Z: Fenster X (schließen) hier implementieren!
    @Override
    public void start(Stage stage) throws IOException {
        //stage in die statische Variable mainStage
        mainStage = stage;
        //Entweder leere BankAccounts/WalletLists oder die aus der Datei geladenen!!!
        BankAccount bankAccount = new BankAccount();
        WalletList walletList = new WalletList();

        try {
            bankAccount = loadBankAccountFromFile();
        } catch (RetrieveDataException e) {
            WalletApp.showErrorDialog("Error on loading BankAccount data. Using new empty account!");
            e.printStackTrace();
        }

        try {
            walletList = loadWalletListFromFile();
        } catch (RetrieveDataException e) {
            WalletApp.showErrorDialog("Error on loading WalletList data. Using new empty WalletList!");
            e.printStackTrace();
        }
        //TEST ob der richtige BankAccount vorhanden ist
        //System.out.println("TEST BANKACCOUNT: "+bankAccount);

        //Video O: Fill GlobalContext = Globaler Speicher
        //Speichern von BankAccount, WalletList und CurrencyPrices
        GlobalContext.getGlobalContext().putStateFor(WalletApp.GLOBAL_WALLET_LIST,walletList);
        GlobalContext.getGlobalContext().putStateFor(WalletApp.GLOBAL_BANK_ACCOUNT,bankAccount);
        GlobalContext.getGlobalContext().putStateFor(WalletApp.GLOBAL_CURRENT_CURRENCY_PRICES,new CurrentCurrencyPrices());

        //Video Z: X Zeichen am Fenster(zum schließen ausblenden)
        //Ein Event konsumieren -> es passiert also nichts!!!
        //Lambda-Ausdruck unten:
        //(event -> event.consume()) ("inputParameter -> Methodenteil der ausgeführt wird!
        mainStage.setOnCloseRequest(event -> event.consume());

        //Über switchScene lässt sich dann die Szene ändern
        WalletApp.switchScene("main.fxml", "at.hakimst.sample.main");

        //Einsprungpunkt in die GUI Entwicklung
        //AnchorPane = Zeichenfläche wo man Anker setzen kann, präsentieren der Sub-ELemente
        //Klasse FXMLLoader mit der Methode: load Laden der fxml-Datei
        //fxml Elemente befinden sich im resources Ordner!
        /*
        AnchorPane root = FXMLLoader.load(WalletApp.class.getResource("main.fxml"),
                ResourceBundle.getBundle("at.hakimst.sample.main"));

        //Erstellung einer Scene auf der wir unser AnchorPane root präsentieren
        //stage.setScene
        //Theater Metapher: Bühne mit mehreren Szenen
        //stage.show
        Scene scene = new Scene(root, 640, 480);
        stage.setScene(scene);
        stage.show(); */
    }
    //Video O: GlobalContext
    // -> 2.Life-Cycle-Method
    // Methode stop: Wenn die Applikation geschlossen wird
    @Override
    public void stop(){
        //Type-Casting (WalletList) da getGlobalContext nur ein Objekt vom Typ "Object" zurückgibt!!!
        WalletList walletList = (WalletList) GlobalContext.getGlobalContext().getStateFor(WalletApp.GLOBAL_WALLET_LIST);
        BankAccount bankAccount = (BankAccount) GlobalContext.getGlobalContext().getStateFor(WalletApp.GLOBAL_BANK_ACCOUNT);
        try {
            storeBankAccountToFile(bankAccount);
            System.out.println("BankAccount Details stored to file!");
            storeWalletListToFile(walletList);
            System.out.println("WalletList Details stored to file!");
        } catch (SaveDataExeption exeption) {
            WalletApp.showErrorDialog("Could not store bankaccount and/or wallet details");
            exeption.printStackTrace();
        }
    }

    public static void main(String[] args) {
        //Zunächst dient die Main-Klasse als TEST-KLASSE!

        //Methode: launch(args)
        //Methode aus der Oberklasse Application
        //Führt verschiedene Methoden aus z.B.: die public void start-Methode
        //start-Methode = Einsprungpunkt in die GUI Entwicklung
        launch(args);

//        //Video C: BankAccount Klasse
//        System.out.println("Hallo Welt");
//        //BankAccount bank = new BankAccount();
//        // Video P:
//        BankAccount bank = (BankAccount) GlobalContext.getGlobalContext().getStateFor(WalletApp.GLOBAL_BANK_ACCOUNT);
//        bank.deposit(new BigDecimal("1000"));
//        System.out.println(bank);
//
////        try {
////            bank.withdraw(new BigDecimal("50"));
////            bank.withdraw(new BigDecimal("51"));
////            System.out.println(bank);
////        } catch(InsufficientBalanceException insufficientBalanceException)
////        {
////            //System.out.println("Zu wenig Geld auf deinem Konto!");
////            System.out.println(insufficientBalanceException.getMessage());
////        }  // Hier könnte noch ein finally-Block stehen
//        // finally Block wird immer ausgeführt, auch wenn eine Exception auftritt!
//
//        //Video D: CryptoCurrencyEnum und Transaction
//        CryptoCurrency crypto = CryptoCurrency.BTC;
//        System.out.println(crypto.getCurrencyName());
//        System.out.println(crypto.getCode());
//        System.out.println(CryptoCurrency.valueOf("BTC").getCurrencyName());
//
//        Transaction transaction = new Transaction(
//                CryptoCurrency.ETH,
//                new BigDecimal("1.23"),
//                new BigDecimal("1567.8"));
//        System.out.println(transaction);
//
//        //Video E: Wallet Klasse Teil 1
//        Wallet wallet = null;
//        try {
//            wallet = new Wallet("My BTC Wallet", CryptoCurrency.BTC, new BigDecimal("1"));
//        } catch (InvalidFeeException e) {                                                //fee in Prozent z.B.: 1%
//            System.out.println(e.getMessage()); // Nur die Nachricht
//            e.printStackTrace(); // Lange Liste wie diese Exception entstanden ist!
//        }
//        System.out.println(wallet);
//
//        //Video F: Wallet Klasse Teil 2
//        try {
//            wallet.buy(new BigDecimal("10"), new BigDecimal("5"), bank);
//        } catch (InvalidAmountException e) {
//            e.printStackTrace();
//        } catch (InsufficientBalanceException insufficientBalanceException) {
//            insufficientBalanceException.printStackTrace();
//        }
//
//        System.out.println(bank);
//        System.out.println(wallet);
//
//        //Video G: Wallet Klasse Teil 3
//        try {
//            wallet.sell(new BigDecimal("10"), new BigDecimal("5"), bank);
//        } catch (InsufficientAmountException e) {
//            e.printStackTrace();
//        } catch (InvalidAmountException e) {
//            e.printStackTrace();
//        }
//
//        System.out.println(bank);
//        System.out.println(wallet);
//
//        //Video H: Wallet-List Klasse
//        //WalletList walletList = new WalletList();
//
//        //Video P:
//        WalletList walletList = (WalletList) GlobalContext.getGlobalContext().getStateFor(WalletApp.GLOBAL_WALLET_LIST);
//        walletList.addWallet(wallet);
//
//        System.out.println(walletList);
//
//        //Video I: HTTP API für Live-Daten
//        //https://www.coingecko.com/api/documentations/v3
//        CurrentPriceForCurrency currentPrices = new CurrentCurrencyPrices();
//        try {
//            BigDecimal result = currentPrices.getCurrentPrice(CryptoCurrency.BTC);
//            System.out.println("API-CALL -> Aktueller Bitcoin-Kurs: " + result);
//            BigDecimal result2 = currentPrices.getCurrentPrice(CryptoCurrency.XRP);
//            System.out.println("API-CALL -> Aktueller XRP-Kurs: " + result2);
//        } catch (GetCurrentPriceException e) {
//            e.printStackTrace();
//        }

        //Video J: Bank-Daten speichern und laden
//        DataStore dataStore = new FileDataStore();
//        try {
//            dataStore.saveBankAccount(bank);
//        } catch (SaveDataExeption e) {
//            System.out.println(e.getMessage());
//        }
//
//        try {
//            BankAccount bankAccount2 = dataStore.retrieveBankAccount();
//            System.out.println(bankAccount2);   //Überprüfen ob es sich um den richtigen BankAccount handelt
//        } catch (RetrieveDataException e) {
//            System.out.println(e.getMessage());
//        }
//
//        //Video K: WalletList in Datei speichern
//        try {
//            dataStore.saveWalletList(walletList);
//        } catch (SaveDataExeption exeption) {
//            exeption.printStackTrace();
//        }
//        try {
//            WalletList walletList2 = dataStore.retrieveWalletList();
//            System.out.println(walletList2);    //Überprüfen ob es sich um die richtige WalletList handelt
//        } catch (RetrieveDataException exeption) {
//            exeption.printStackTrace();
//        }

        // START MIT DER GRAFISCHEN BENUTZEROBERFLÄCHE GUI

        //Video L: GUI - Short Intro to FXML-GUIs in JAVA

        // -> Einführung in JAVA_FX: Ordner resources = main.css, main.fxml, main.properties, styles.css
        // -> MainController für die Implementierung der Logik


        //Video M: Haupt-App-Klasse zum Hochfahren der GUI-Applikation
        // siehe oben!!!
        // -> Main Klasse umbenannt in: WalletApp
        // -> WalletApp ist unere Bootstrap-Datei (Haupteinsprungspunkt)


        //Video N: Haupt-App-Klasse (WalletApp) zum Hochfahren der GUI-Applikation TEIL 2
        // Filehandling
        // siehe oben!!!


        //Video O: Global State / Global Context
        // Neues Package: ui (User-Interface)
        // TEST siehe oben!!!


        //Video P: Haupt-App-Klasse (WalletApp) zum Hochfahren der GUI-Applikation TEIL 3
        // siehe oben in der public static void main!!!
        // Methode launch von ganz unten nach oben positionieren (Life-Cycle-Methods)


        //Video Q: main.fxml
        // Bearbeitung siehe Klasse: main.fxml
        // Bearbeitung siehe Klasse: main.css


        //Video R: main.fxml TEIl 2
        // Bearbeitung siehe Klasse: main.fxml
        // Bearbeitung siehe Klasse: main.css


        //Video S: MainController (Code Behind)
        // Bearbeitung siehe Klasse: MainController
        // Erstellung der Mutterklass BaseControllerState


        //Video T: MainController & TableView und WalletList
        // Bearbeitung siehe Klasse: MainController


        //Video U: MainController & TableView und WalletList TEIL 2
        // Bearbeitung siehe Klasse: MainController

        //Video V: wallet.fxml
        // Bearbeitung siehe Klasse: wallet.css (Kopie von main.css machen)
        // Bearbeitung siehe Klasse: wallet.fxml (Kopie von main.fxml machen)
        // Bearbeitung siehe Klasse: wallet.properties (Kopie von main.properties machen)
        // Bearbeitung siehe Klasse: Klasse WalletController erstellen

        //Video W: WalletController
        // Bearbeitung siehe Klasse: WalletController
        // Button BackToMain ausprogrammieren -> mit Hilfe: initialize-Methode
        // Datenfeld: Klasse WalletApp
        // Klasse: MainControleller Methode: openWallet()

        //Video X: WalletController Teil 2

        //Video Y: WalletController Teil 3

        //Video Z: WalletController Teil 4 ABSCHLUSS!!!
        // Bearbeitung siehe Klasse: WalletController Methode: buy()
        // Bearbeitung siehe Klasse: WalletController Methode: sell()
        // Bearbeitung siehe Klasse: WalletApp oben (EXIT BUTTON / X FENSTER SCHLIEßEN)
    }
}
