package domain;

import Exceptions.InsufficientBalanceException;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;

// BankAccount in Datei speichern, darum implements Serializable
// = Marker-Interface (Klasse kann serialisiert werden, sprich BankAccount in eine Datei speichern)
public class BankAccount implements Serializable {

    private BigDecimal balance; //Viele Nachkommastellen und ganz genau

    public BankAccount(){
        this.balance = new BigDecimal("0").setScale(2, RoundingMode.HALF_UP); // setScale ist für das Runden zuständig, Half-Up: Ab der Hälfte aufrunden!
        //BigDecimal result = this.balance.add(new BigDecimal("2")).setScale(2,RoundingMode.HALF_UP);
    }

    public BigDecimal getBalance() {
        return this.balance;
    }

    /**
     * Einzahlungsmethode deposit (TEST 2021-09-28)
     * @param amount
     */
    public void deposit(BigDecimal amount)
    {
        if(amount != null) {
            this.balance = this.balance.add(amount).setScale(2, RoundingMode.HALF_UP);
        }
    }
    /**
     * Auszahlungsmethode withdraw
     * throws InsufficientBalanceException
     * @param amount
     */
    public void withdraw(BigDecimal amount) throws InsufficientBalanceException //Schlüsselwort "throws"
    {                                                                           //Methode withdraw kann evtl. eine Exception auslösen!
        if(amount != null){
            if(this.balance.subtract(amount).doubleValue() >= 0) //compareTo(new BigDecimal("0")) >=0 würde auch funktionieren!
            {
                this.balance = this.balance.subtract(amount).setScale(2,RoundingMode.HALF_UP);
            } else {
                //Ansonsten Exception schmeißen = Ausnahme produzieren
                // New Package Exceptions erstellen -> erben/extends Exception
                throw new InsufficientBalanceException(); //Neue Instanz der InsufficientBalanceException
            }
        }
    }

    @Override
    public String toString() {
        return "BankAccount{" +
                "balance=" + balance +
                '}';
    }
}
