package domain;

import Exceptions.RetrieveDataException;
import Exceptions.SaveDataExeption;

public interface DataStore {

    void saveBankAccount (BankAccount bankAccount) throws SaveDataExeption;
    void saveWalletList (WalletList walletList) throws SaveDataExeption;
    BankAccount retrieveBankAccount() throws RetrieveDataException; // BankAccount wieder zurückholen!
    WalletList retrieveWalletList() throws RetrieveDataException; //Von der Datei laden sozzusagen

}
