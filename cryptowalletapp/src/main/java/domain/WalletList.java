package domain;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

public class WalletList implements Serializable {

    private final HashMap<CryptoCurrency,Wallet> wallets;

    public WalletList() {
        this.wallets = new HashMap<>();
    }

    /**
     * Wallet hinzufügen
     * Methode: addWallet
     * @param wallet
     */
    public void addWallet(Wallet wallet)
    {   //Business-Logik: Überprüfung ob das Wallet bereits exisiert! Immer nur ein Wallet pro Currency!
        if(wallet != null && !this.wallets.containsKey(wallet.getCryptoCurrency()))
        {
            this.wallets.put(wallet.getCryptoCurrency(),wallet); //Einfügen/Put in die HashMap!
        }
    }

    public Wallet getWallet(CryptoCurrency cryptoCurrency)
    {
        return this.wallets.get(cryptoCurrency);
    }

    public List<Wallet> getWalletsAsObservableList() //Wird später für die Anzeige in der GUI benötigt!
    {
        return wallets.values().stream().collect(Collectors.toList()); //Hilfsmethode zum Holen der Werte. Aus Datenstrom machen wir eine Liste!
    }                                                                  //Den Teil "Wallet" als List zurück!

    @Override
    public String toString() {
        return "WalletList{" +
                "wallets=" + wallets +
                '}';
    }
}
