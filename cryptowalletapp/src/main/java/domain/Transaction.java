package domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.util.UUID;

public class Transaction implements Serializable {

    private final UUID id; // Unique Identifier bietet Java an! Typ UUID (lange Zahl) Automatisches Generieren von IDs
    private final CryptoCurrency cryptoCurrency;
    private final BigDecimal amount; //Menge wie viel gekauft wird
    private final BigDecimal priceAtTransactionDate; //Preis zum Transaktionsdatum
    private final LocalDate date;
    private final BigDecimal total; //Gesamtwert: Preis x Menge

    /**
     * Konstruktor bekommt die CryptoCurreny, die Menge und den aktuellen Preis mit
     * @param cryptoCurrency
     * @param amount
     * @param priceAtTransactionDate
     */
    public Transaction(CryptoCurrency cryptoCurrency, BigDecimal amount, BigDecimal priceAtTransactionDate) {
        this.id = UUID.randomUUID(); //Random Klasse der UUID generiert eine zufällige ID
        this.cryptoCurrency = cryptoCurrency;
        this.amount = amount;
        this.priceAtTransactionDate = priceAtTransactionDate;
        this.date = LocalDate.now();
        this.total = this.amount.multiply(this.priceAtTransactionDate).setScale(6, RoundingMode.HALF_UP);
    }

    public CryptoCurrency getCryptoCurrency() {
        return cryptoCurrency;
    }

    public UUID getId() {
        return id;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public BigDecimal getPriceAtTransactionDate() {
        return priceAtTransactionDate;
    }

    public LocalDate getDate() {
        return date;
    }

    public BigDecimal getTotal() {
        return total;
    }

    @Override
    public String toString() {
        return "Transaction{" +
                "id=" + id +
                ", cryptoCurrency=" + cryptoCurrency +
                ", amount=" + amount +
                ", priceAtTransactionDate=" + priceAtTransactionDate +
                ", date=" + date +
                ", total=" + total +
                '}';
    }
}
