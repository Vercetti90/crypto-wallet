package Infrastructure;

import Exceptions.RetrieveDataException;
import Exceptions.SaveDataExeption;
import domain.BankAccount;
import domain.DataStore;
import domain.WalletList;

import java.io.*;

public class FileDataStore implements DataStore {
    @Override
    public void saveBankAccount(BankAccount bankAccount) throws SaveDataExeption {
        //Daten in eine Datei schreiben, damit beim nächsten Mal die Daten wieder zu Verfügung stehen -> ObjectOutputStream
        //kann Klassen entgegennehmen!
        //benötigt als Parameter einen FileOutputStream(Hilfsklasse) -> dieser benötigt wiederrum die Angabe einer Datei, in die die Daten gespeichert werden hier: account.bin
        //.bin da es sicher hierbei um Binärdaten handelt!
        if (bankAccount != null) {   //NULL CHECK!!!
            ObjectOutputStream objectOutputStream = null;
            try {
                objectOutputStream = new ObjectOutputStream(new FileOutputStream("account.bin"));
                objectOutputStream.writeObject(bankAccount);    //An dieser Stelle könnten bei allen IO-Exception auftreten (sprich 3x try-catch-Block)
                objectOutputStream.close(); // File-Handling zu machen!
            } catch (IOException ioException) {
                ioException.printStackTrace();
                throw new SaveDataExeption("Error saving BankAccount to file: " + ioException.getMessage()); //Damit wir wissen was das Problem gewesen ist!
            }
        }
    }

    @Override
    public void saveWalletList(WalletList walletList) throws SaveDataExeption {
        //WalletList in Datei speichern
        //NULL CHECK!!!
        if (walletList != null) {
            ObjectOutputStream objectOutputStream = null;
            try {
                objectOutputStream = new ObjectOutputStream((new FileOutputStream("walletlist.bin")));
                objectOutputStream.writeObject(walletList);
                objectOutputStream.close();
            } catch (IOException ioException) {
                ioException.printStackTrace();
                throw new SaveDataExeption("Error saving WalletList to file: " + ioException.getMessage()); //Damit wir wissen was das Problem gewesen ist!
            }
        }
    }

    @Override
    public BankAccount retrieveBankAccount() throws RetrieveDataException {
        //Aus einer Datei lesen
        ObjectInputStream objectInputStream = null;
        try {
            objectInputStream = new ObjectInputStream(new FileInputStream("account.bin"));
            BankAccount bankAccount = (BankAccount) objectInputStream.readObject(); //Überprüfung von .readObject()
            objectInputStream.close();
            return bankAccount;
        } catch (IOException | ClassNotFoundException exception) { //schöner 2 seperate Catch-Blöcke
            exception.printStackTrace();
            throw new RetrieveDataException("Error on retrieving BankAccount data from file!: " + exception.getMessage());
        }
        //Object o = objectInputStream.readObject(); Explizite Prüfung ob es sich um ein BankAccount Objekt handelt!
        //if(o instanceof BankAccount)
    }

    @Override
    public WalletList retrieveWalletList() throws RetrieveDataException {
        ObjectInputStream objectInputStream = null;
        try {
            objectInputStream = new ObjectInputStream(new FileInputStream("walletlist.bin"));
            WalletList walletList = (WalletList) objectInputStream.readObject();
            objectInputStream.close();
            return walletList;
        } catch (IOException | ClassNotFoundException exception) {
            exception.printStackTrace();
            throw new RetrieveDataException("Error on retrieving WalletList data from file!: " + exception.getMessage());
        }
    }
}
